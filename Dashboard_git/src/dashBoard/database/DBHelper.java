package dashBoard.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container;
import com.vaadin.data.util.sqlcontainer.SQLContainer;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.data.util.sqlcontainer.connection.SimpleJDBCConnectionPool;
import com.vaadin.data.util.sqlcontainer.query.FreeformQuery;

import dashBoard.model.Project;
import dashBoard.model.User;
/**
 * permet le dialogue entre l'application et la base de donn�es
 * @author tbuisine
 *
 */
public class DBHelper {
	
	private JDBCConnectionPool pool;
	
	/** 
	 * initialisation du DBHelper
	 */
	public DBHelper(){
		try {
			pool = new SimpleJDBCConnectionPool("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/dashboard", "root", "Natas91");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/** 
	 * permet l'ajout d'un nouveaux utilisateur
	 * @param username
	 * @param password
	 * @throws SQLException
	 */
	public void insertUser(String username, String password) throws SQLException{
		
		
		Connection conn = pool.reserveConnection();
        Statement statement = conn.createStatement();
        statement.execute("insert into utilisateur (nom,motdepasse) values('"+username+"' , '"+password+"')");
        
        conn.commit();
        conn.close();
	}
	/** 
	 * recupere les infos d'un utilisateur
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public List<User> getUser(String user) throws SQLException{
		List<User> utilisateur  = new ArrayList<User>() ;
		Connection conn = pool.reserveConnection();
		Statement statement = conn.createStatement();
		ResultSet rs = statement.executeQuery( "select * from utilisateur where nom = '"+ user + "'" );
		while ( rs.next() ) {
			
			utilisateur.add(new User((Integer)rs.getObject(1), String.valueOf(rs.getObject(2)), String.valueOf(rs.getObject(3))));
					
		               
		}
		        
		conn.close();
		
		return utilisateur;
	}
	
	
	/**
	 * recupere les url de projet pour un utilisateur 
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public Container getProjectURLbyUser(String username) throws SQLException{
		String q = "select urlproject from project , utilisateur as util where util.idutilisateur = project.iduser  and nom = '"+ username + "'";
		FreeformQuery query  = new FreeformQuery(q, pool,"urlproject");
		Container container = new SQLContainer(query);		
		return container;
	}
	
	/**
	 * recupere les projets d'un utilisateur
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public Container getProjectbyUser(String username) throws SQLException{
		String q = "select * from project , utilisateur as util where util.idutilisateur = project.iduser  and nom = '"+ username + "'";
		FreeformQuery query  = new FreeformQuery(q, pool,"idproject");
		Container container = new SQLContainer(query);		
		return container;
	}
	/**
	 * ajoute un nouveau project pour un utilisateur
	 * @param idUser
	 * @param name
	 * @param url
	 * @return
	 * @throws SQLException
	 */
	public int insertProjectbyUser(int idUser, String name, String url) throws SQLException{
		int id = 0;
		Connection conn = pool.reserveConnection();
        Statement statement = conn.createStatement();
        statement.executeUpdate("insert into project (nameproject,urlproject,iduser) values('"+name+"' , '"+url+"' , "+idUser+")",Statement.RETURN_GENERATED_KEYS);  
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()){
           id = rs.getInt(1);
        }

        
        conn.commit();
        conn.close();
        return id;
	}
	/**
	 * supprime un projet pour un utilisateur
	 * @param idProject
	 * @throws SQLException
	 */
	public void deleteProject(int idProject) throws SQLException{
		Connection conn = pool.reserveConnection();
        Statement statement = conn.createStatement();
        statement.executeUpdate("delete from project where idproject = " + idProject );  
        conn.commit();
        conn.close();
	}
	/** 
	 * mets a jou les information d'un projet
	 * @param idProject
	 * @param name
	 * @param url
	 * @throws SQLException
	 */
	public void updateProject(int idProject, String name, String url) throws SQLException{
		Connection conn = pool.reserveConnection();
        Statement statement = conn.createStatement();
        statement.executeUpdate("update project set nameproject = '"+name+"' , urlproject = '" + url +"' where idproject ="+ idProject   );  
        conn.commit();
        conn.close();
	}
	/** 
	 * recupere la list des projets
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public List<Project> getListProject(String username) throws SQLException{
			List<Project> project  = new ArrayList<Project>() ;
			Connection conn = pool.reserveConnection();
			Statement statement = conn.createStatement();
			ResultSet rs = statement.executeQuery( "select urlproject from project , utilisateur as util where util.idutilisateur = project.iduser  and nom = '"+ username + "'" );
			while ( rs.next() ) {
				
					project.add(new Project( String.valueOf(rs.getObject(1))));
			               
			}
			        
			conn.close();
			
			return project;
		}
	
	}
