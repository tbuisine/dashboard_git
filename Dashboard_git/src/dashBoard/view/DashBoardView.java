package dashBoard.view;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import dashBoard.data.Issues;
import dashBoard.data.IssuesByStateChart;
import dashBoard.data.IssuesNumberDuringTimeChart;
import dashBoard.data.VelocityByProject;
import dashBoard.data.velocitybycontributor;
import dashBoard.database.DBHelper;
import dashBoard.model.Project;
/** 
 * la dashboard prenant en compte un utilisateur loguer ou non 
 * @author tbuisine
 *
 */
public class DashBoardView extends VerticalLayout implements View {

    Table t;
    DBHelper db = new DBHelper();
    List<Project>   projects = new ArrayList<Project>();
    public DashBoardView()   {
    	
        setSizeFull();
        addStyleName("dashboard-view");

        HorizontalLayout top = new HorizontalLayout();
        top.setWidth("100%");
        top.setSpacing(true);
        top.addStyleName("toolbar");
        addComponent(top);
        final Label title = new Label("My Dashboard");
        title.setSizeUndefined();
        title.addStyleName("h1");
        top.addComponent(title);
        top.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
        top.setExpandRatio(title, 1);
   	 	TextField projectTF = new TextField();
   	 	ComboBox projectCB = new ComboBox();
   	 	//  si il n'est pas connect�
        if(((String)getUI().getCurrent().getSession().getAttribute("user")).equals("null")){
        		projects.add(new Project("https://github.com/junit-team/junit"));
       // si il est connect�
        }else{
        	try {
				projects.addAll(db.getListProject((String)getUI().getCurrent().getSession().getAttribute("user")));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

        	

        	
        }
        Issues issues = new Issues();
        for(Project project : projects){
        	project.setIssues(issues.getIssuesByProject(project));
        }
      
        Button edit = new Button();
        edit.addStyleName("icon-edit");
        edit.addStyleName("icon-only");
        top.addComponent(edit);
        edit.setDescription("Edit Dashboard");
        edit.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                final Window w = new Window("Edit Dashboard");

                w.setModal(true);
                w.setClosable(false);
                w.setResizable(false);
                w.addStyleName("edit-dashboard");

                getUI().addWindow(w);
                //TODO
                w.setContent(new VerticalLayout() {
                    TextField name = new TextField("Dashboard Name");
                    {
                        addComponent(new FormLayout() {
                            {
                                setSizeUndefined();
                                setMargin(true);
                                name.setValue(title.getValue());
                                addComponent(name);
                                name.focus();
                                name.selectAll();
                            }
                        });

                        addComponent(new HorizontalLayout() {
                            {
                                setMargin(true);
                                setSpacing(true);
                                addStyleName("footer");
                                setWidth("100%");

                                Button cancel = new Button("Cancel");
                                cancel.addClickListener(new ClickListener() {
                                    @Override
                                    public void buttonClick(ClickEvent event) {
                                        w.close();
                                    }
                                });
                                cancel.setClickShortcut(KeyCode.ESCAPE, null);
                                addComponent(cancel);
                                setExpandRatio(cancel, 1);
                                setComponentAlignment(cancel,
                                        Alignment.TOP_RIGHT);

                                Button ok = new Button("Save");
                                ok.addStyleName("wide");
                                ok.addStyleName("default");
                                ok.addClickListener(new ClickListener() {
                                    @Override
                                    public void buttonClick(ClickEvent event) {
                                        title.setValue(name.getValue());
                                        w.close();
                                    }
                                });
                                ok.setClickShortcut(KeyCode.ENTER, null);
                                addComponent(ok);
                            }
                        });

                    }
                });

            }
        });
        top.setComponentAlignment(edit, Alignment.MIDDLE_LEFT);

        HorizontalLayout row = new HorizontalLayout();
        row.setSizeFull();
        row.setMargin(new MarginInfo(true, true, false, true));
        row.setSpacing(true);
        addComponent(row);
        setExpandRatio(row, 1.5f);
        
        row.addComponent(createPanel(new IssuesNumberDuringTimeChart(projects)));

       
        
        row.addComponent(createPanel(new velocitybycontributor(projects)));

        row = new HorizontalLayout();
        row.setMargin(true);
        row.setSizeFull();
        
        
        row.setSpacing(true);
        addComponent(row);
        setExpandRatio(row, 2);

       
        

        row.addComponent(createPanel(new VelocityByProject(projects)));

       row.addComponent(createPanel(new IssuesByStateChart(projects)));

    }
    /**
     * construction d'un panel qui contiendra un graphe
     * @param content
     * @return
     */
    private CssLayout createPanel(Component content) {
        CssLayout panel = new CssLayout();
        panel.addStyleName("layout-panel");
        panel.setSizeFull();

       

        panel.addComponent(content);
        return panel;
    }





	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

}
