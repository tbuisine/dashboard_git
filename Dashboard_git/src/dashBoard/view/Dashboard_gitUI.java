package dashBoard.view;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.event.Transferable;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import dashBoard.database.DBHelper;

/**
 * la vue principal dirigeant les divers vue et contenant le menu
 * @author tbuisine
 *
 */
@SuppressWarnings("serial")
@Theme("dashboard")
public class Dashboard_gitUI extends UI {
	
	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = Dashboard_gitUI.class, widgetset="dashBoard.MyWidgetSet")
	public static class Servlet extends VaadinServlet {
	}

	    private static final long serialVersionUID = 1L;

	    CssLayout root = new CssLayout();

	    VerticalLayout loginLayout;

	    CssLayout menu = new CssLayout();
	    CssLayout content = new CssLayout();
	    private Navigator nav;

	    private HelpManager helpManager;
	    private DBHelper db;
	    HashMap<String, Class<? extends View>> routes = new HashMap<String, Class<? extends View>>() {
	        {
	        	put("/login",Login.class);
	            put("/dashboard",  DashBoardView.class);
	        }
	    };
	    HashMap<String, Button> viewNameToMenuButton = new HashMap<String, Button>();
	    /**
		 * initialisation de la vue
		 * @author tbuisine
		 *
		 */
	@Override
	protected void init(VaadinRequest request) {
		 setLocale(Locale.FRANCE);
	        helpManager = new HelpManager(this);
	         db = new DBHelper();
	        setContent(root);
	        root.addStyleName("root");
	        root.setSizeFull();
	        getSession().setAttribute("user", "null");
	        // Unfortunate to use an actual widget here, but since CSS generated
	        // elements can't be transitioned yet, we must
	        Label bg = new Label();
	        bg.setSizeUndefined();
	        bg.addStyleName("login-bg");
	        root.addComponent(bg);
	       // buildLoginView(false);
	        buildMainView(false);

	}
	/**
	 * construction de la vue pour un utilisateur non loguer
	 * @param exit
	 */
	
	private void buildMainView(boolean exit){
		 if (exit) {
			 root.removeAllComponents();
		 }
		 initNavigator(routes);
         helpManager.closeAll();
         initMenu();
         for (final String view : new String[] { "login", "dashboard"   }) {
        	 Button b = new NativeButton(view.substring(0, 1).toUpperCase()+ view.substring(1).replace('-', ' '));
	         b.addStyleName("icon-" + view);
	         b.addClickListener(new ClickListener() {
	        	 @Override
	             public void buttonClick(ClickEvent event) {
	        		 clearMenuSelection();
	        		 event.getButton().addStyleName("selected");
	        		 if (!nav.getState().equals("/" + view))
	        			 nav.navigateTo("/" + view);
	        	 }	
	         });

	         menu.addComponent(b);
	         viewNameToMenuButton.put("/" + view, b);
         }
         menu.addStyleName("menu");
         menu.setHeight("100%");

         viewNameToMenuButton.get("/dashboard").setHtmlContentAllowed(true);
         viewNameToMenuButton.get("/dashboard").setCaption(
        		 "Dashboard<span class=\"badge\">2</span>");

         String f = Page.getCurrent().getUriFragment();
         if (f != null && f.startsWith("!")) {
        	 f = f.substring(1);
         }
         if (f == null || f.equals("") || f.equals("/")) {
        	 nav.navigateTo("/login");
        	 menu.getComponent(0).addStyleName("selected");
        	 helpManager.showHelpFor(Login.class);
         } else {
        	 nav.navigateTo(f);
        	 helpManager.showHelpFor(routes.get(f));
        	 viewNameToMenuButton.get(f).addStyleName("selected");
         }

         nav.addViewChangeListener(new ViewChangeListener() {

        	 @Override
        	 public boolean beforeViewChange(ViewChangeEvent event) {
        		 helpManager.closeAll();
        		 return true;
        	 }

        	 @Override
        	 public void afterViewChange(ViewChangeEvent event) {
        		 View newView = event.getNewView();
        		 helpManager.showHelpFor(newView);

        	 }
         });

	}
	private Transferable items;
	private void clearMenuSelection() {
		for (Iterator<Component> it = menu.getComponentIterator(); it.hasNext();) {
			Component next = it.next();
			if (next instanceof NativeButton) {
				next.removeStyleName("selected");
			} else if (next instanceof DragAndDropWrapper) {
				// Wow, this is ugly (even uglier than the rest of the code)
				((DragAndDropWrapper) next).iterator().next().removeStyleName("selected");
            }
        }
    }

   

   /** initialisation du menu de gauche
    * 
    */
	private void initMenu(){
		final String user  = getSession().getAttribute("user").toString();
		root.addComponent(new HorizontalLayout() {
			{
				setSizeFull();
                addStyleName("main-view");
                addComponent(new VerticalLayout() {
                    // Sidebar
                    {
                        addStyleName("sidebar");
                        setWidth(null);
                        setHeight("100%");

                        // Branding element
                        addComponent(new CssLayout() {
                            {
                                addStyleName("branding");
                                Label logo = new Label(
                                        "<span>Git</span> Dashboard",
                                        ContentMode.HTML);
                                logo.setSizeUndefined();
                                addComponent(logo);
                                // addComponent(new Image(null, new
                                // ThemeResource(
                                // "img/branding.png")));
                            }
                        });

                        // Main menu
                        addComponent(menu);
                        setExpandRatio(menu, 1);

                        // User menu
                        if(!user.equals("null")){
	                        addComponent(new VerticalLayout() {
	                            {
	                                setSizeUndefined();
	                                addStyleName("user");
		                                Label userName = new Label(user);	
		                                userName.setSizeUndefined();
		                                addComponent(userName);
		                            
	                                Command cmd = new Command() {
	                                    @Override
	                                    public void menuSelected(
	                                            MenuItem selectedItem) {
	                                        Notification
	                                                .show("Not implemented in this demo");
	                                    }
	                                };
	                                
	
	                                Button exit = new NativeButton("Exit");
	                                exit.addStyleName("icon-cancel");
	                                exit.setDescription("Sign Out");
	                                addComponent(exit);
	                                exit.addClickListener(new ClickListener() {
	                                    @Override
	                                    public void buttonClick(ClickEvent event) {
	                                    	getSession().setAttribute("user", "null");
	                                    	buildMainView(true);
	                                    }
	                                });
	                            }
	                        });
                        }
                    }
                });
                // Content
                addComponent(content);
                content.setSizeFull();
                content.addStyleName("view-content");
                setExpandRatio(content, 1);
            }

        });

        menu.removeAllComponents();
    }
   
    /** initialisation du navigateur par defaut
     * 
     * @param routeur
     */
    private void initNavigator(HashMap<String, Class<? extends View>> routeur){
    	nav = new Navigator(this, content);
		 for (String route : routeur.keySet()) {
	            nav.addView(route, routeur.get(route));
	        }
		 
    }
    /** 
     * construction du menu pour un utilisateur loguer
     * @param isLog
     */
	public void buildmainViewWithUser(boolean isLog){
			 root.removeAllComponents();
			 HashMap<String, Class<? extends View>> routesbis = new HashMap<String, Class<? extends View>>() {
			        {
			            put("/dashboard",  DashBoardView.class);
			            put("/setting", SettingView.class);
			        }
			    };
			     initNavigator(routesbis);
		         helpManager.closeAll();
		         initMenu();
		         for (final String view : new String[] {  "dashboard","setting"   }) {
		        	 Button b = new NativeButton(view.substring(0, 1).toUpperCase()+ view.substring(1).replace('-', ' '));
			         b.addStyleName("icon-" + view);
			         b.addClickListener(new ClickListener() {
			        	 @Override
			             public void buttonClick(ClickEvent event) {
			        		 clearMenuSelection();
			        		 event.getButton().addStyleName("selected");
			        		 if (!nav.getState().equals("/" + view))
			        			 nav.navigateTo("/" + view);
			        	 }	
			         });

			         menu.addComponent(b);
			         viewNameToMenuButton.put("/" + view, b);
		         }
		         menu.addStyleName("menu");
		         menu.setHeight("100%");

		         viewNameToMenuButton.get("/dashboard").setHtmlContentAllowed(true);
		         viewNameToMenuButton.get("/dashboard").setCaption(
		        		 "Dashboard<span class=\"badge\">2</span>");
		         String f ="";
		         if(!isLog){
		          f = Page.getCurrent().getUriFragment();
		         }
		         if (f != null && f.startsWith("!")) {
		        	 f = f.substring(1);
		         }
		         if (f == null || f.equals("") || f.equals("/")) {
		        	 nav.navigateTo("/setting");
		        	 menu.getComponent(0).addStyleName("selected");
		        	 helpManager.showHelpFor(SettingView.class);
		         } else {
		        	 nav.navigateTo(f);
		        	 helpManager.showHelpFor(routesbis.get(f));
		        	 viewNameToMenuButton.get(f).addStyleName("selected");
		         }

		         nav.addViewChangeListener(new ViewChangeListener() {

		        	 @Override
		        	 public boolean beforeViewChange(ViewChangeEvent event) {
		        		 helpManager.closeAll();
		        		 return true;
		        	 }

		        	 @Override
		        	 public void afterViewChange(ViewChangeEvent event) {
		        		 View newView = event.getNewView();
		        		 helpManager.showHelpFor(newView);

		        	 }
		         });

			    }
	}

    
	

