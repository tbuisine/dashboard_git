package dashBoard.view;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import dashBoard.database.DBHelper;
import dashBoard.model.User;


/** vue pour le login
 * 
 * @author tbuisine
 *
 */
public class Login  extends VerticalLayout implements View{
	
    CssLayout root = new CssLayout();
    DBHelper db;
	public Login(){
        db = new DBHelper();
        initUI();
	}
	private void initUI(){
		

		this.setSizeFull();
		this.addStyleName("login-layout");
		root.addComponent(this);

		final CssLayout loginPanel = new CssLayout();
		loginPanel.addStyleName("login-panel");

		HorizontalLayout labels = new HorizontalLayout();
		labels.setWidth("100%");
		labels.setMargin(true);
		labels.addStyleName("labels");
		loginPanel.addComponent(labels);

		Label welcome = new Label("Welcome");
		welcome.setSizeUndefined();
		welcome.addStyleName("h4");
		labels.addComponent(welcome);
		labels.setComponentAlignment(welcome, Alignment.MIDDLE_LEFT);

		Label title = new Label("Git Dashboard");
		title.setSizeUndefined();
		title.addStyleName("h2");
		title.addStyleName("light");
		labels.addComponent(title);
		labels.setComponentAlignment(title, Alignment.MIDDLE_RIGHT);

		HorizontalLayout fields = new HorizontalLayout();
		fields.setSpacing(true);
		fields.setMargin(true);
		fields.addStyleName("fields");

		final TextField username = new TextField("Username");
		username.focus();
		fields.addComponent(username);

		final PasswordField password = new PasswordField("Password");
		fields.addComponent(password);

		final Button signin = new Button("Sign In");
		signin.addStyleName("default");
		fields.addComponent(signin);
		fields.setComponentAlignment(signin, Alignment.BOTTOM_LEFT);

		final ShortcutListener enter = new ShortcutListener("Sign In",
				KeyCode.ENTER, null) {
			@Override
			public void handleAction(Object sender, Object target) {
				signin.click();
			}
		};

		signin.addClickListener(new ClickListener() {
  
			@Override
			public void buttonClick(ClickEvent event) {
				List<User> user=new ArrayList<User>();
				try {
					user = db.getUser(username.getValue());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (!user.isEmpty() && user.get(0).getPassword().equals(password.getValue())) {
					signin.removeShortcutListener(enter);
					getSession().setAttribute("user", username.getValue());
					getSession().setAttribute("iduser", user.get(0).getId());
					Dashboard_gitUI dash  = (Dashboard_gitUI) UI.getCurrent();
					dash.buildmainViewWithUser(true);
				} else {
					if (loginPanel.getComponentCount() > 2) {
						// Remove the previous error message
						loginPanel.removeComponent(loginPanel.getComponent(2));
					}
					// Add new error message
					Label error = new Label(
							"Wrong username or password.",
							ContentMode.HTML);
					error.addStyleName("error");
					error.setSizeUndefined();
					error.addStyleName("light");
					// Add animation
					error.addStyleName("v-animate-reveal");
					loginPanel.addComponent(error);
					username.focus();
				}
			}
		});

		final Button signup = new Button("Sign Up");
		signup.addStyleName("default");
		fields.addComponent(signup);
		fields.setComponentAlignment(signup, Alignment.BOTTOM_LEFT);

		final ShortcutListener upEnter = new ShortcutListener("Sign Up", KeyCode.ENTER, null) {
				  @Override
				  public void handleAction(Object sender, Object target) {
				      signup.click();
				  }
				};
		
		signup.addClickListener(new ClickListener() {
		  @Override
		  public void buttonClick(ClickEvent event) {
		  	try {
					db.insertUser(username.getValue(), password.getValue());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		  }
		});

		signin.addShortcutListener(enter);

		loginPanel.addComponent(fields);

		this.addComponent(loginPanel);
		this.setComponentAlignment(loginPanel, Alignment.MIDDLE_CENTER);
	}	
	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

	}


