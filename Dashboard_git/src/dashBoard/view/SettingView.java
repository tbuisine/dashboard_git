package dashBoard.view;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import dashBoard.database.DBHelper;
/**
 * vue pour ajouter/ modifier/supprimer un url pour un utilisateur connect�
 * @author tbuisine
 *
 */
public class SettingView extends HorizontalLayout implements View{

	private Table projectList = new Table();
	private Button addNewProjectButton = new Button("New");
	private Button removeProjectButton = new Button("Remove this contact");
	private Button saveProjectButton  = new Button("save");
	private FormLayout editorLayout = new FormLayout();
	private FieldGroup editorFields = new FieldGroup();
	private static final String PNAME = "nameproject";
	private static final String PURL = "urlproject";
	private DBHelper db;
	private static final Object[] fieldNames = new Object[] {PNAME, PURL};
	private Container projectcontainer;
	private int idUser;
	private String nameUser;
	private TextField projectName;
	private TextField projectUrl;
	private Boolean forAdd = false;
	private List<Integer> add = new ArrayList<Integer>();
	public SettingView(){
		db = new DBHelper();
		initUI();
		nameUser = (String)getUI().getCurrent().getSession().getAttribute("user");
		idUser = (Integer)getUI().getCurrent().getSession().getAttribute("iduser");
		initProjectList();
		initEditor();
		initAddRemoveButtons();
		initSaveButtons();
	}
	private void initUI(){
		this.setSizeFull();

		VerticalLayout leftLayout = new VerticalLayout();
		this.addComponent(leftLayout);
		this.addComponent(editorLayout);
		leftLayout.addComponent(projectList);
		HorizontalLayout bottomLeftLayout = new HorizontalLayout();
		leftLayout.addComponent(bottomLeftLayout);
		bottomLeftLayout.addComponent(addNewProjectButton);

		/* Set the contents in the left of the split panel to use all the space */
		leftLayout.setSizeFull();

		/*
		 * On the left side, expand the size of the contactList so that it uses
		 * all the space left after from bottomLeftLayout
		 */
		leftLayout.setExpandRatio(projectList, 1);
		projectList.setSizeFull();
		/*
		 * In the bottomLeftLayout, searchField takes all the width there is
		 * after adding addNewContactButton. The height of the layout is defined
		 * by the tallest component.
		 */
		bottomLeftLayout.setWidth("100%");
		bottomLeftLayout.setExpandRatio(addNewProjectButton, 1);

		/* Put a little margin around the fields in the right side editor */
		editorLayout.setMargin(true);
		editorLayout.setVisible(false);
		
	}
	
	private void initProjectList(){
		projectList.addStyleName("plain");
        projectList.addStyleName("borderless");
		try {
			projectcontainer =  db.getProjectbyUser(nameUser);
			projectList.setContainerDataSource(projectcontainer);
		} catch (SQLException e) {

			e.printStackTrace();
		}
		projectList.setVisibleColumns(new String[]{ PNAME,PURL});
		projectList.setSelectable(true);
		projectList.setImmediate(true);

		projectList.addValueChangeListener(new Property.ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				Object projectId = projectList.getValue();

				/*
				 * When a contact is selected from the list, we want to show
				 * that in our editor on the right. This is nicely done by the
				 * FieldGroup that binds all the fields to the corresponding
				 * Properties in our contact at once.
				 */
				if (projectId != null)
					editorFields.setItemDataSource(projectList
							.getItem(projectId));

				editorLayout.setVisible(projectId != null);
			}
		});
	}
	private void initEditor() {

		editorLayout.addComponent(removeProjectButton);
		editorLayout.addComponent(saveProjectButton);
		editorLayout.setComponentAlignment(removeProjectButton, Alignment.TOP_RIGHT);
		editorLayout.setComponentAlignment(saveProjectButton, Alignment.TOP_LEFT);

			projectName = new TextField(PNAME);
			projectUrl = new TextField(PURL);
			editorLayout.addComponent(projectName);
			editorLayout.addComponent(projectUrl);
			editorLayout.setComponentAlignment(projectName, Alignment.MIDDLE_CENTER);
			editorLayout.setComponentAlignment(projectUrl, Alignment.MIDDLE_CENTER);
			projectName.setWidth("100%");
			projectUrl.setWidth("100%");

			/*
			 * We use a FieldGroup to connect multiple components to a data
			 * source at once.
			 */
			editorFields.bind(projectName, PNAME);
			editorFields.bind(projectUrl, PURL);
		

		/*
		 * Data can be buffered in the user interface. When doing so, commit()
		 * writes the changes to the data source. Here we choose to write the
		 * changes automatically without calling commit().
		 */
		editorFields.setBuffered(false);
	}
	
	private void initAddRemoveButtons() {
		addNewProjectButton.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				/*
				 * Rows in the Container data model are called Item. Here we add
				 * a new row in the beginning of the list.
				 */
				Object ProjectId = projectcontainer.addItem();

				/*
				 * Each Item has a set of Properties that hold values. Here we
				 * set a couple of those.
				 */
				projectList.getContainerProperty(ProjectId, PNAME ).setValue("New");
				projectList.getContainerProperty(ProjectId, PURL).setValue("URL");

				/* Lets choose the newly created contact to edit it. */
				projectList.select(ProjectId);
				/* save in database*/
				try {
					int id = db.insertProjectbyUser(idUser, "New", "URL");
					projectList.getContainerProperty(ProjectId, "idproject" ).setReadOnly(false);
					projectList.getContainerProperty(ProjectId, "idproject" ).setValue(id);
					projectList.getContainerProperty(ProjectId, "idproject" ).setReadOnly(true);

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				

			}
		});

		removeProjectButton.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				Object projectId = projectList.getValue();
				try {
					db.deleteProject( Integer.valueOf(projectId.toString()));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				projectList.removeItem(projectId);
			}
		});
	}
	private void initSaveButtons() {
		saveProjectButton.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				Object projectId = projectList.getValue();
				projectList.getContainerProperty(projectId, PNAME ).setValue(projectName.getValue());
				projectList.getContainerProperty(projectId, PURL).setValue(projectUrl.getValue());
				projectList.refreshRowCache();
				try {
						db.updateProject(Integer.valueOf(projectId.toString()), projectName.getValue(), projectUrl.getValue());
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					try {
						db.updateProject((Integer) projectList.getContainerProperty(projectId, "idproject").getValue(), projectName.getValue(), projectUrl.getValue());
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
	
	
	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getNameUser() {
		return nameUser;
	}
	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

}
