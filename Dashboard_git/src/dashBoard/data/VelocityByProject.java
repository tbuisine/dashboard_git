package dashBoard.data;

import java.util.List;

import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;

import dashBoard.model.Project;

/**
 * permet l'affichage de la velocit� par projet
 * @author tbuisine
 *
 */
public class VelocityByProject extends Chart{
	
	public VelocityByProject(List<Project> projects){
		setCaption("velocity by project");
        getConfiguration().setTitle("");
        getConfiguration().getChart().setType(ChartType.COLUMN);
        getConfiguration().getLegend().setEnabled(false);
        setWidth("500px");
        setHeight("200px");
        for(Project project : projects){
        	getConfiguration().addSeries(new Issues().getVeloByProject(project));
        }
	}
}
