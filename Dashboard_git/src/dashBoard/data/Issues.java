package dashBoard.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import retrofit.RestAdapter;

import com.vaadin.addon.charts.model.DataSeries;
import com.vaadin.addon.charts.model.DataSeriesItem;
import com.vaadin.addon.charts.model.PlotOptionsSpline;
import com.vaadin.addon.charts.model.style.SolidColor;

import dashBoard.model.Project;

/**
 * permet le dialogue 
 * @author tbuisine
 *
 */
public class Issues implements Comparable<Issues> {

	String title;
	Date created_at;
	Date closed_at;
	UserGit user;
	
	/**
	 * recupere la liste des issues pour un projet
	 * @param url
	 * @return
	 */
	public List<Issues> getIssuesByProject(Project url){
		int occurs1  = url.getUrl().indexOf("/", 8);
	
		System.out.println(occurs1);
		int occurs2  = url.getUrl().lastIndexOf("/");
		System.out.println(occurs2);

		String site  =  url.getUrl().substring(0,occurs1);
		System.out.println(site);
		String owner = url.getUrl().substring(occurs1+1, occurs2);
		System.out.println(owner);
		String project = url.getUrl().substring(occurs2+1, url.getUrl().length());
		System.out.println(project);
		url.setName(project);

		RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(
				"https://api.github.com").build();
		// Create an instance of our GitHub API interface.
				GitHub github = restAdapter.create(GitHub.class);
				// Fetch and print a list of the contributors to this library.
				int page = 1;
		List<Issues> issues = github.issues(owner, project,String.valueOf(1),"all");
		while(issues.size()/(100)==page){
			page++;
			issues.addAll(github.issues(owner, project,String.valueOf(page),"all"));
		}
		return issues;
		
	}
	
	/**
	 * permet la recup�ration du nombre d'issue par date pour l'affichage dans le graphe
	 * @param project
	 * @return
	 */
	public DataSeries getNumberIssuesByDate(Project project) {
			List<Issues> issues = project.getIssues();
			DataSeries series = new DataSeries(project.getName());
			series.setPlotOptions(new PlotOptionsSpline());
			Collections.sort(issues);
			int nb = 0;
			List<Date> closed = new ArrayList<Date>();
			//System.out.println(issues.size());
			for (int cpt = 0; cpt < issues.size(); cpt++) {
				List<Integer> idToRemove = new ArrayList<Integer>();
				for(int idx = 0; idx < issues.size();idx++){
					
					if(issues.get(idx).closed_at!= null && issues.get(idx).closed_at.before(issues.get(cpt).created_at)&&issues.get(idx).closed_at.after(issues.get(cpt-1).created_at)){
						nb--;
						series.add(new DataSeriesItem(closed.get(idx), nb));
				//		System.out.println(issues.get(idx).title + " closed: " +issues.get(idx).created_at);
					}
				}
				
	
				nb++;
				//System.out.println(issues.get(cpt).title + " create: " +issues.get(cpt).created_at);
				series.add(new DataSeriesItem(issues.get(cpt).created_at, nb));
	
				if (issues.get(0).closed_at != null) {
					closed.add(issues.get(cpt).closed_at);
				}
			}
			return series;
		}
		
	

	/**
	 * permet la recup�ration de la v�locit� pour un projet
	 * @param project
	 * @return
	 */
	public DataSeries getVeloByProject(Project project){
		
		DataSeries series = new DataSeries("velocity");
		series.add(new DataSeriesItem(project.getName(), project.getIssues().size()));
		return series;
	}
	
	/**
	 * permet de recuperer les �tats des issues pour tout les projets reunie
	 * @param projects
	 * @return
	 */
	public DataSeries getIssuesByState(List<Project> projects){
		List<Issues> issues = new ArrayList<Issues>();
		DataSeries series = new DataSeries();
		for(Project project : projects ){
			issues.addAll(project.getIssues());
		}
		int closed = 0;
		int open  = 0;
		for(Issues issue: issues){
			if (issue.closed_at != null){
				closed++;
			}else{
				open++;
			}
		}
		
		series.add(new DataSeriesItem("OPEN", open));
		series.add(new DataSeriesItem("CLOSE",closed));
		
		return series;
		
	}
		
	/**
	 * recupere la v�locit� d'un contributeur pour les divers projets
	 * @param projects
	 * @return
	 */
	public List<DataSeries> velocityByContributor(List<Project> projects){
		List<SolidColor> color = new ArrayList<SolidColor>();
		Random rand = new Random();
		List<String> users = new ArrayList<String>();
		List<DataSeries> series = new ArrayList<DataSeries>();
		for(Project project : projects){
			for(Issues issue : project.getIssues()){
				if( issue.closed_at != null){
					Boolean isInList = false;
					for(String user : users){
						if(user.equals(issue.user.login)){
							isInList = true;
						}
					}
					if(!isInList){
						users.add(issue.user.login);
					}
				}
			}
			color.add(new SolidColor(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256)));
		}
		for(String user : users){
			DataSeries serie = new DataSeries(user);
			int idx = 0;

			for(Project project : projects){

				int compteur = 0;
				for(Issues issue : project.getIssues()){
					if( issue.closed_at  != null && user.equals(issue.user.login) ){
						compteur++;
					
					}
				}
			serie.add(new DataSeriesItem(project.getName(),compteur,color.get(idx) ));

			idx++;

			}
			series.add(serie);

		}
		return series;
	}
	
	
	/** permet le tri par date
	 * 
	 */
	public int compareTo(Issues arg0) {
		return this.created_at.compareTo(arg0.created_at);
	}

}
