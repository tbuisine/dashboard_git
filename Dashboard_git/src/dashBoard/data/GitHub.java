package dashBoard.data;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * L'interface qui permet le dialogue avec le REST de Github
 * @author tbuisine
 *
 */
public interface GitHub {
	@GET("/repos/{owner}/{repo}/issues?per_page=100")
	List<Issues> issues(@Path("owner") String owner,
			@Path("repo") String repo,@Query("page")String id,@Query("state") String state);
}
