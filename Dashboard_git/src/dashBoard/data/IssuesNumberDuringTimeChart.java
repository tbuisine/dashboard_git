package dashBoard.data;

import java.util.List;

import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.AxisType;
import com.vaadin.addon.charts.model.ChartType;

import dashBoard.model.Project;

/**
 * affiche la courbe d'Úvolution du nombre d'issues ouverte
 * @author tbuisine
 *
 */
public class IssuesNumberDuringTimeChart extends Chart{
	
	
	public IssuesNumberDuringTimeChart(List<Project> projects){
		setCaption("Issues by project");
        getConfiguration().setTitle("Issues by project");
        getConfiguration().getChart().setType(ChartType.LINE);
        getConfiguration().getxAxis().setType(AxisType.DATETIME);
        getConfiguration().getLegend().setEnabled(false);
        setWidth("100%");
        setHeight("90%");
        
        Issues issues = new Issues();
        for(Project project : projects){
        	getConfiguration().addSeries(issues.getNumberIssuesByDate(project));
        }
        
	}

}
