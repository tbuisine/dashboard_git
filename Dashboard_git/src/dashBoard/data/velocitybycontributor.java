package dashBoard.data;

import java.util.List;

import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.DataSeries;

import dashBoard.model.Project;
/**
 * permet l'affichage de la v�locit� des contrubuteur par projet
 * @author tbuisine
 *
 */
public class velocitybycontributor extends Chart{

	public velocitybycontributor(List<Project> projects){
	    getConfiguration().getChart().setType(ChartType.COLUMN);

//		PlotOptionsColumn plotOptions = new PlotOptionsColumn();
//        plotOptions.setStacking(Stacking.PERCENT);
//        getConfiguration().setPlotOptions(plotOptions);
        Issues issues = new Issues();
        for(DataSeries series :issues.velocityByContributor(projects)){
            getConfiguration().addSeries(series);

        }
	}
}
