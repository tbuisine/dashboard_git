package dashBoard.data;

import java.util.List;

import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;

import dashBoard.model.Project;

/**
 * affiche le camembert  representant les �tats des issues
 * @author tbuisine
 *
 */
public class IssuesByStateChart extends Chart{
	
	public IssuesByStateChart(List<Project> projects){
	setCaption("Issue By State");
    getConfiguration().getChart().setType(ChartType.PIE);
    getConfiguration().getxAxis().getLabels().setEnabled(false);
    setWidth("500px");
    setHeight("200px");
    getConfiguration().getxAxis().setTickWidth(0);
    Issues issues = new Issues();
    getConfiguration().setSeries(issues.getIssuesByState(projects));
	}

}
