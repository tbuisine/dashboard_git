package dashBoard.model;

import java.io.Serializable;
import java.util.List;

import dashBoard.data.Issues;
/**
 * un projet github
 * @author tbuisine
 *
 */
public class Project implements Serializable {
	private int idProject;
	private String name;
	private String url;
	private List<Issues> issues;
	
	private int idUser;
	public Project(String url) {
		
		this.url = url;
		
		}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdProject() {
		return idProject;
	}
	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}
	public List<Issues> getIssues() {
		return issues;
	}
	public void setIssues(List<Issues> issues) {
		this.issues = issues;
	}
}
